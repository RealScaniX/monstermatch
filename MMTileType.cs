public enum MMTileType {
    EMPTY,
    WALL,
    EXIT,
    ENTRANCE,
    MONSTERTRACK
}