public enum LightMode {
    DEFAULT,
    PANIC,
    LOST_SIGHT,
    LOVE,
    HATE,
    FINISHED
}