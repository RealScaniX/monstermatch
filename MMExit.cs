using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MMExit : MMTileObject {

    private bool opened;

    protected override void Awake() {
        base.Awake();

    }

    protected override void Update() {
        base.Update();

        bool shouldBeOpen = MMGame.instance.monsters.Length <= 1;
        if (opened != shouldBeOpen) {
            opened = shouldBeOpen;

            // change visuals
            transform.GetChild(0).gameObject.SetActive(!opened);
            transform.GetChild(1).gameObject.SetActive(opened);
        }
    }

    public bool IsOpen() {
        return opened;
    }

    public override bool Blocks(int x, int y) {
        return false;
    }
}
