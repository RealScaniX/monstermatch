using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MMDoor : MMTileObject {

    private bool opened;

    private float yOffset;
    private Vector3 startPos;

    protected override void Awake() {
        base.Awake();
        startPos = tx.localPosition;
    }

    protected override void Update() {
        base.Update();

        bool shouldBeOpen = MMGame.instance.monsters.Length == 0 || MMGame.instance.TileOccupied(x, y) is MMPlayer;
        foreach (MMMonster m in MMGame.instance.monsters) {
            if (m.Distance(this) < 3) {
                shouldBeOpen = true;
                break;
            }
        }
        opened = shouldBeOpen;

        yOffset = Mathf.Max(0, Mathf.Min(0.75f, yOffset + (shouldBeOpen ? 1 : -1) * 4.0f * 0.8f * Time.deltaTime));
        tx.localPosition = startPos + Vector3.down * yOffset;
    }

    public override bool Blocks(int x, int y) {
        return opened ? false : base.Blocks(x, y);
    }
}
