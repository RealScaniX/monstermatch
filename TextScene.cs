﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class TextScene : MonoBehaviour {

    public bool ending;

    private float startTime;

    // Use this for initialization
    void Start () {
		startTime = Time.time;
        MMGame.levelNumber = 0;
	}

	// Update is called once per frame
	void Update () {
        if (Time.time - startTime > 1) {
            if (Input.GetKeyDown(KeyCode.Alpha1) || Input.GetKeyDown(KeyCode.Keypad1))
                MMGame.OpenLink("https://www.patreon.com/posts/compass-sf-20407502");
            else if (Input.GetKeyDown(KeyCode.Alpha2) || Input.GetKeyDown(KeyCode.Keypad2))
                MMGame.OpenLink("https://twitter.com/somepx");
            else if (Input.GetKeyDown(KeyCode.Alpha3) || Input.GetKeyDown(KeyCode.Keypad3))
                MMGame.OpenLink("https://twitter.com/HiBeGame");
            // else if (Input.GetKeyDown(KeyCode.Escape))
            //     Application.Quit();
            else if (Input.anyKeyDown) {
                MMGame.levelNumber = 0;
                SceneManager.LoadScene("game");
            }
        }
	}
}
