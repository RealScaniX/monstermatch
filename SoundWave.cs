﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SoundWave : MonoBehaviour {
    [SerializeField]
    private float fadeTime = 1.0f;
    [SerializeField]
    private Color colorStart = Color.white;
    [SerializeField]
    private Color colorEnd = Color.black;
    [SerializeField]
    private Color colorEmissionStart = Color.white;
    [SerializeField]
    private Color colorEmissionEnd = Color.black;

    private Transform tx;
    private Vector3 baseScale;
    private float startTime = -1;
    private MMTileObject tileObj;
    private MeshRenderer mr;
    private MaterialPropertyBlock mpb;

    void Awake() {
        tx = transform;
        baseScale = tx.localScale;
    }

    public void Begin() {
        startTime = Time.time;
        mr = GetComponent<MeshRenderer>();
        mpb = new MaterialPropertyBlock();
        float s = Mathf.Cos(-0.75f);
        tx.localScale = baseScale * s;
	}

	void Update () {
        if (startTime < 0)
            return;
        float f = Mathf.Min(1, Mathf.Pow((Time.time - startTime) / fadeTime, 0.5f));
        float s = Mathf.Cos(-0.75f + f * (Mathf.PI / 2 + 0.75f));
        tx.localScale = baseScale * s;
        mpb.SetColor("_Color", Color.Lerp(colorStart, colorEnd, f));
        mpb.SetColor("_EmissionColor", Color.Lerp(colorEmissionStart, colorEmissionEnd, f));
        mr.SetPropertyBlock(mpb);
        if (f == 1)
            Destroy(gameObject);
	}
}
