﻿using System;
using System.Collections;
using System.Collections.Generic;
#if UNITY_EDITOR
using UnityEditor;
using UnityEditor.SceneManagement;
#endif
using UnityEngine;

public class MMLevel : MonoBehaviour {
    public const int LEVEL_SIZE = 30;

    #if UNITY_EDITOR
    [CustomEditor(typeof(MMLevel))]
    public class BookAngleEditor : Editor {
        public override void OnInspectorGUI() {
            base.OnInspectorGUI();
            MMLevel level = (MMLevel)target;
            if (GUILayout.Button("Clear")) {
                level.CleanUp();
            }
            if (GUILayout.Button("Build")) {
                level.Build();
            }
            if (GUILayout.Button("Grab")) {
                level.Grab();
            }
        }
    }

#endif

    [SerializeField]
    private MMGame game;

    [SerializeField]
    internal string message;

    [SerializeField]
    internal bool finished;

    [SerializeField]
    private bool hideObjects = true;

    [SerializeField]
    private bool[] rightTurners = new bool[6];

    [SerializeField]
    [Multiline(40)]
    private string levelText;

    [SerializeField]
    private string doors;

    void Start () {

	}

	void Update () {

	}

    public void CleanUp() {
        StartChange();
        Transform rootTx = transform;
        for (int i=rootTx.childCount-1; i>=0; i--) {
            Remove(rootTx.GetChild(i).gameObject);
        }
        game.player = null;
        game.monsters = new MMMonster[0];
        EndChange();
    }

    public void Build() {
        StartChange();
        CleanUp();

        if (!Application.isPlaying) {
            // Clean up other levels
            foreach (MMLevel l in game.levels)
                if (l != this) l.CleanUp();
        }

        if (string.IsNullOrEmpty(levelText))
            return;

        List<MMMonster> monsters = new List<MMMonster>();
        for (int y=0; y<LEVEL_SIZE; y++) {
            for (int x=0; x<LEVEL_SIZE; x++) {
                char t = Tile(x, y, true);
                if (t == '#') {
                    PlaceObject<MMWall>(game.wallPrefab, x, y, 0);
                } else if (t == '*') {
                    game.player = PlaceObject<MMPlayer>(game.playerPrefab, x, y, 0);
                    PlaceObject<MMTileObject>(game.entrancePrefab, x, y, 1);
                } else if (t == ':' || t == '.') {
                    PlaceObject<MMTileObject>(game.monsterTrackPrefab, x, y, 0);
                } else if (t == '>') {
                    MMExit exit = PlaceObject<MMExit>(game.exitPrefab, x, y, 0);
                    if (Application.isPlaying) {
                        if (Tile(x-1, y) != '#') {
                            exit.direction = 1;
                        } else if (Tile(x+1, y) != '#') {
                            exit.direction = 3;
                        } else if (Tile(x, y-1) != '#') {
                            exit.direction = 2;
                        }
                    }
                } else if (t >= '1' && t <= '9') {
                    // get direction from neighbor tile
                    int direction = 0;
                    if (Tile(x+1, y, true) == ':')
                        direction = 1;
                    else if (Tile(x, y-1, true) == ':')
                        direction = 2;
                    else if (Tile(x-1, y, true) == ':')
                        direction = 3;
                    int heartType = t - '1';
                    int monsterDesign = Application.isPlaying ? UnityEngine.Random.Range(0, game.monsterPrefabs.Length) : heartType;
                    MMMonster monster = PlaceObject<MMMonster>(game.monsterPrefabs[monsterDesign], x, y, direction);
                    monster.rightTurner = rightTurners.Length > monsters.Count && rightTurners[monsters.Count];
                    monsters.Add(monster);

                    // Add monster extras
                    //-horns or schleife

                    // Add heart
                    if (Application.isPlaying) {
                        GameObject heartGO = GameObject.Instantiate<GameObject>(game.heartPrefab);
                        heartGO.transform.parent = transform;
                        MMHeart heart = heartGO.GetComponent<MMHeart>();
                        heart.heartType = heartType;
                        heart.monster = monster;
                        monster.heart = heart;
                    }

                    PlaceObject<MMTileObject>(game.monsterTrackPrefab, x, y, 0);
                }
            }
        }
        game.monsters = monsters.ToArray();

        // Doors
        List<MMDoor> doorsList = new List<MMDoor>();
        if (!string.IsNullOrEmpty(this.doors)) {
            string[] doors = this.doors.Split(',');
            foreach (string door in doors) {
                if (string.IsNullOrEmpty(door))
                    continue;
                string[] inf = door.Split('.');
                doorsList.Add(PlaceObject<MMDoor>(game.doorPrefab, int.Parse(inf[0]), int.Parse(inf[1]), int.Parse(inf[2])));
            }
        }
        game.doors = doorsList.ToArray();

        EndChange();
    }

    public char Tile(int x, int y, bool includeTracks = false) {
        char tile = '#';

        string[] rows = levelText.Split(new char[]{'\n'}, 100, StringSplitOptions.RemoveEmptyEntries);

        if (y >= 0 && y < rows.Length && x >= 0 && x < rows[rows.Length - 1 - y].Length) {
            tile = rows[rows.Length - 1 - y][x];

            if (!includeTracks && tile == ':')
                tile = '.';
        }

        return tile;
    }

    public T PlaceObject<T>(GameObject prefab, int x, int y, int direction) where T : MMTileObject {
        GameObject o = Instantiate<GameObject>(prefab);
        o.SetActive(true);
        o.name = o.name.Replace("(Clone)", "_" + x + "." + y);
        o.transform.SetParent(transform);


        o.transform.localPosition = new Vector3(x, o.transform.localPosition.y, y);
        //o.transform.localRotation = o.transform.localRotation * Quaternion.Euler(new Vector3(0, Mathf.Repeat(direction * 90, 360), 0));
        //o.transform.localEulerAngles = o.transform.localEulerAngles + new Vector3(0, Mathf.Repeat(direction * 90, 360), 0);

        if (hideObjects)
            o.transform.hideFlags = HideFlags.HideInHierarchy;

        // if (Application.isPlaying) {
        //     MMTileObject tileObj = o.GetComponent<MMTileObject>();
        //     if (tileObj != null) {
        //         tileObj.direction = direction;
        //         tileObj.x = x;
        //         tileObj.y = y;
        //     }
        // }

        T tile = o.AddComponent<T>();
        tile.GrabBaseRotation(prefab.transform);
        tile.direction = direction;

        // tile.GrabBaseRotation(prefab.transform);

        return tile;
    }

    private void Grab() {
        if (finished) {
            Debug.LogError("Level is finished!");
            return;
        }
        Transform rootTx = transform;
        if (rootTx.childCount < 20) {
            Debug.LogError("Level is empty!");
            return;
        }

        StartChange();

        string wallPrefix = game.wallPrefab.name.ToLower();
        string playerPrefix = game.playerPrefab.name.ToLower();
        string exitPrefix = game.exitPrefab.name.ToLower();
        string entrancePrefix = game.entrancePrefab.name.ToLower();
        string doorPrefix = game.doorPrefab.name.ToLower();
        string monsterTrackPrefix = game.monsterTrackPrefab.name.ToLower();
        string[] monsterPrefix = new string[game.monsterPrefabs.Length];
        for (int i=0; i<game.monsterPrefabs.Length; i++) {
            monsterPrefix[i] = game.monsterPrefabs[i].name.ToLower();
        }

        char[][] tiles = new char[LEVEL_SIZE][];
        for (int i=0; i<tiles.Length; i++)
            tiles[i] = new char[LEVEL_SIZE];
        string doors = "";
        bool playerFound = false;
        for (int i=0; i<rootTx.childCount; i++) {
            Transform t = rootTx.GetChild(i);
            string name = t.gameObject.name.ToLower();
            Vector2Int pos = new Vector2Int(Mathf.RoundToInt(t.localPosition.x), Mathf.RoundToInt(t.localPosition.z));
            if (pos.x < 0 || pos.x > tiles.Length || pos.y < 0 || pos.y > tiles[0].Length) {
                Debug.LogError("Invalid tile position for object: " + name);
#if UNITY_EDITOR
                UnityEditor.Selection.activeGameObject = t.gameObject;
#endif
                return;
            }
            if (name.StartsWith(wallPrefix)) {
                tiles[pos.x][pos.y] = '#';
                continue;
            } else if (name.StartsWith(monsterTrackPrefix)) {
                if (tiles[pos.x][pos.y] == 0 || tiles[pos.x][pos.y] == ' ')
                    tiles[pos.x][pos.y] = '.';
                continue;
            } else if (name.StartsWith(playerPrefix)) {
                if (playerFound && tiles[pos.x][pos.y] != '*') {
                    Debug.LogError("Second player/entrance found!");
#if UNITY_EDITOR
                    UnityEditor.Selection.activeGameObject = t.gameObject;
#endif
                    return;
                }
                playerFound = true;
                tiles[pos.x][pos.y] = '*';
                continue;
            } else if (name.StartsWith(entrancePrefix)) {
                if (playerFound && tiles[pos.x][pos.y] != '*') {
                    Debug.LogError("Second player/entrance found!");
#if UNITY_EDITOR
                    UnityEditor.Selection.activeGameObject = t.gameObject;
#endif
                    continue;
                }
                playerFound = true;
                tiles[pos.x][pos.y] = '*';
                continue;
            } else if (name.StartsWith(exitPrefix)) {
                tiles[pos.x][pos.y] = '>';
                continue;
            } else if (name.StartsWith(doorPrefix)) {
                int dir = MMTileObject.DirectionByAngle(t.localEulerAngles.y);
                if (doors.Length > 0)
                    doors += ",";
                doors += pos.x + "." + pos.y + "." + dir;
                continue;
            } else if (name.StartsWith("monster")) {
                int monsterType = -1;
                float baseAngle = 0;
                for (int j=0; j<game.monsterPrefabs.Length; j++) {
                    if (name.StartsWith(monsterPrefix[j])) {
                        monsterType = j;
                        baseAngle = game.monsterPrefabs[j].transform.localEulerAngles.y;
                        break;
                    }
                }
                if (monsterType > -1) {
                    tiles[pos.x][pos.y] = (char)('1' + monsterType);
                    int dir = MMTileObject.DirectionByAngle(t.localEulerAngles.y, baseAngle);
                    Vector2Int vec = MMTileObject.DirectionVector(dir);
                    tiles[pos.x+vec.x][pos.y+vec.y] = ':';
                    continue;
                }
            }

            Debug.LogError("Unknown tile: " + name);
#if UNITY_EDITOR
            UnityEditor.Selection.activeGameObject = t.gameObject;
#endif
            return;
        }

        // Build string
        string level = "";
        for (int y=tiles.Length-1; y>=0; y--) {
            for (int x=0; x<tiles[y].Length; x++) {
                level += tiles[x][y] != 0 ? tiles[x][y] : ' ';
            }
            level += "\n";
        }
        levelText = level;
        this.doors = doors;
#if UNITY_EDITOR
        // UnityEditor.Selection.activeGameObject = Camera.main.gameObject;
        // UnityEditor.Selection.activeGameObject = gameObject;
#endif
        EndChange();
    }

    private void StartChange() {
#if UNITY_EDITOR
        if (!Application.isPlaying) {
            Undo.RecordObject(gameObject, "Level text change");
            EditorSceneManager.MarkAllScenesDirty();
            EditorUtility.SetDirty(gameObject);
        }
#endif
    }

    private void EndChange() {
#if UNITY_EDITOR
        if (!Application.isPlaying) {
            EditorSceneManager.MarkAllScenesDirty();
            EditorUtility.SetDirty(gameObject);
        }
#endif
    }

    private void Remove(GameObject obj) {
#if UNITY_EDITOR
        if (!Application.isPlaying) {
            DestroyImmediate(obj);
        } else
#endif
        {
            Destroy(obj);
        }
    }
}
