using System;
using System.Collections.Generic;
using UnityEngine;

public class Pathfinder {
    private class PathNode {
        public readonly int x;
        public readonly int y;
        private readonly List<PathNode> path;

        public PathNode(int x, int y, List<PathNode> path) {
            this.x = x;
            this.y = y;
            this.path = new List<PathNode>(path);
        }

        public List<PathNode> GetPath() {
            List<PathNode> path = new List<PathNode>(this.path);
            path.Add(this);
            return path;
        }

        internal List<Vector2Int> SimplePath() {
            List<Vector2Int> spath = new List<Vector2Int>();
            List<PathNode> path = GetPath();
            foreach (PathNode n in path)
                spath.Add(new Vector2Int(n.x, n.y));
            return spath;
        }
    }

    internal static List<Vector2Int> FindPath(MMGame game, MMMonster monster, MMTileObject target, int x1, int y1, int x2, int y2) {
        HashSet<int> processed = new HashSet<int>();
        List<PathNode> checkStack = new List<PathNode>();
        checkStack.Add(new PathNode(x1, y1, new List<PathNode>()));
        int i = 0;
        while (i++ < 10000 && checkStack.Count > 0) {
            PathNode node = checkStack[0];
            checkStack.RemoveAt(0);

            // Mark processed
            int x = node.x, y = node.y;
            if (processed.Contains(x + y * 1000))
                continue;
            processed.Add(x + y * 1000);

            // Wall?
            bool targetFound = x == x2 && y == y2;
            MMTileObject obj = game.TileOccupied(x, y, -1, game.player);
            if (obj is MMWall || ((obj is MMMonster || obj is MMPlayer) && (!targetFound && obj != monster && obj != target)))
                continue;

            // Found target?
            if (targetFound)
                return node.SimplePath();

            // Add checks for surrounding fields
            List<PathNode> path = node.GetPath();
            checkStack.Add(new PathNode(x, y+1, path));
            checkStack.Add(new PathNode(x+1, y, path));
            checkStack.Add(new PathNode(x, y-1, path));
            checkStack.Add(new PathNode(x-1, y, path));
        }
        return null;
    }
}