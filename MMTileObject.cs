﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MMTileObject : MonoBehaviour {
    public const float MOVE_TIME = 0.4f;

    public int x {
        get {
            if (x_ == -1)
                x_ = Mathf.RoundToInt(tx.localPosition.x);
            return x_;
        }
        set {
            tx.localPosition = Pos(value, y);
            x_ = -1;
            targetX = x;
        }
    }
    public int y {
        get {
            if (y_ == -1)
                y_ = Mathf.RoundToInt(tx.localPosition.z);
            return y_;
        }
        set {
            tx.localPosition = Pos(x, value);
            y_ = -1;
            targetY = y;
        }
    }

    public Vector2Int position {
        get {
            return new Vector2Int(x, y);
        }
    }

    public Vector3 position3d {
        get {
            return Pos(x, y);
        }
    }

    public Vector2Int target {
        get {
            return new Vector2Int(targetX, targetY);
        }
    }

    public int direction {
        get {
            if (direction_ == -1) {
                float angle = transform.localEulerAngles.y;
                direction_ = DirectionByAngle(angle, baseRotation.y);
            }
            return direction_;
        }
        set {
            transform.localEulerAngles = baseRotation + new Vector3(0, value * 90, 0);
            direction_ = -1;
        }
    }

    protected Transform tx;
    protected int targetX;
    protected int targetY;
    private bool baseRotationGrabbed;
    private Vector3 baseRotation;
    private int x_ = -1;
    private int y_ = -1;
    private float moveStart = -1;
    private int direction_ = -1;
    private float lastMoveTime;

    public static int RotateDirection(int direction, int rotation) {
        while (rotation < 0)
            rotation += 4;
        return (direction + rotation) % 4;
    }

    public static int DirectionByAngle(float angle, float baseRotation = 0) {
        float r = Mathf.Repeat(angle - baseRotation, 360);
        return Mathf.FloorToInt(r / 90);
    }

    public static int DirectionByVector(Vector2Int vec) {
        return DirectionByVector(vec.x, vec.y);
    }

    public static int DirectionByVector(int dx, int dy) {
        return dx != 0 ? (dx < 0 ? 3 : 1) : (dy < 0 ? 2 : 0);
    }

    public static Vector2Int DirectionVector(int direction) {
        Vector2Int vec = Vector2Int.zero;
        switch (direction) {
            case 0:
                vec.y += 1;
                break;
            case 1:
                vec.x += 1;
                break;
            case 2:
                vec.y -= 1;
                break;
            case 3:
                vec.x -= 1;
                break;
        }
        return vec;
    }

    public Vector2Int DirectionVector() {
        return DirectionVector(this.direction);
    }

    public void LookAt(MMTileObject other) {
        direction = DirectionByVector(other.position - position);
    }

    public bool isMoving {
        get {
            return moveStart > -1 && (targetX != x || targetY != y);
        }
    }

    public Vector3 Pos(int x, int y) {
        Vector3 pos = tx.localPosition;
        pos.x = x;
        pos.z = y;
        return pos;
    }

    public void MoveTowards(int direction) {
        this.direction = direction;
        Vector2Int vec = DirectionVector(direction);
        MoveTo(x + vec.x, y + vec.y);
    }

    public void MoveTo(int x, int y) {
        direction = DirectionByVector(new Vector2Int(x - this.x, y - this.y));
        targetX = x;
        targetY = y;
        if (Time.time - lastMoveTime < 0.1f)
            moveStart = lastMoveTime;
        else
            moveStart = Time.time;
    }

    protected virtual void Awake() {
        tx = transform;
        targetX = x;
        targetY = y;
        GrabBaseRotation(tx);
    }

    public void GrabBaseRotation(Transform tx) {
        if (baseRotationGrabbed)
            return;
        baseRotationGrabbed = true;
        baseRotation = tx.localEulerAngles;
    }

    protected virtual void Update() {
        if (isMoving) {
            float f = Mathf.Min(1, (Time.time - moveStart) / MOVE_TIME);
            tx.localPosition = Vector3.Lerp(Pos(x, y), Pos(targetX, targetY), f);
            if (f == 1) {
                x = targetX;
                y = targetY;
                lastMoveTime = moveStart + MOVE_TIME;
                moveStart = -1;
            }
        }
    }

    public float Distance(MMTileObject other) {
        float dx = other.x - x;
        float dy = other.y - y;
        return Mathf.Sqrt(dx*dx + dy*dy);
        //return Math.Max(Math.Abs(other.x - x), Math.Abs(other.y - y));
    }

    public virtual bool Blocks(int x, int y) {
        return (x == this.x && y == this.y) || (x == this.targetX && y == this.targetY);
    }
}
