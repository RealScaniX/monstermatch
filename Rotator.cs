﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Rotator : MonoBehaviour {
    private Transform tx;
    private float startTime;
    private MMTileObject tileObj;

    [SerializeField]
    private Vector3 rotation = Vector3.zero;

    void Start () {
		tx = transform;
        startTime = Time.time;
        tileObj = GetComponentInParent<MMTileObject>();
	}

	void Update () {
        float f = (Time.time - startTime);
        Vector3 rot = new Vector3(
            Mathf.Repeat(rotation.x * f, 360),
            Mathf.Repeat(rotation.y * f, 360),
            Mathf.Repeat(rotation.z * f, 360));
		tx.localEulerAngles = rot;
        bool occupied = tileObj != null && MMGame.instance.TileOccupied(tileObj.x, tileObj.y) != null;
        tx.localPosition = new Vector3(0, (occupied ? 1 : 0) + Mathf.Max(-0.75f, Mathf.Sin(f * 2) * 0.75f), 0);
	}
}
