﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class OneShotSound : MonoBehaviour {

    private bool started;

    private AudioSource audioSource;
    private float delay;

    public static OneShotSound PlaySound(AudioClip clip, MMTileObject emitter, float threeDness, float volume, float pitch, float delay) {
        if (clip == null)
            return null;
        GameObject obj = new GameObject("OneShot." + clip.name);
        obj.transform.position = emitter != null ? emitter.position3d : Vector3.zero;

        AudioSource audioSource = obj.AddComponent<AudioSource>();
        audioSource.clip = clip;
        audioSource.playOnAwake = false;
        audioSource.pitch = pitch;
        audioSource.volume = volume * volume;
        audioSource.spatialBlend = threeDness;

        OneShotSound oss = obj.AddComponent<OneShotSound>();
        oss.delay = delay;
        oss.audioSource = audioSource;

        return oss;
    }

    void Start () {
        started = true;
        if (delay > 0)
            audioSource.PlayDelayed(delay);
        else
            audioSource.Play();
	}

	void Update () {
        if (started && !audioSource.isPlaying) {
            Destroy(gameObject);
        }
	}
}
