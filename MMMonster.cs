﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MMMonster : MMTileObject {

    public const float VIEW_DISTANCE_MONSTER = 2.0f;
    public const float VIEW_DISTANCE_PLAYER = 4.0f;

    public enum MonsterMode {
        PATROL,
        CHASING,
        FROZEN,
        IN_LOVE,
        ATTACK,
        LOST_SIGHT
    }

    internal bool roundSight;

    internal MMHeart heart;

    internal MonsterMode mode = MonsterMode.PATROL;
    private MMTileObject modeTarget;
    private int lostSightRotation;
    private float modeStartTime;
    private Stack<Vector2Int> chaseStack = new Stack<Vector2Int>();

    private List<Vector2Int> pathToPOI = new List<Vector2Int>();

    private Vector2Int currentPOI = Vector2Int.zero;
    private int index_ = -1;

    internal bool rightTurner;

    public int heartType {
        get {
            return heart != null ? heart.heartType : 0;
        }
    }

    public int index {
        get {
            if (index_ == -1) {
                index_ = 0;
                foreach (MMMonster m in MMGame.instance.monsters) {
                    if (m == this)
                        break;
                    index_++;
                }
            }
            return index_;
        }
    }

    public bool triggered {
        get {
            return mode == MonsterMode.CHASING || mode == MonsterMode.LOST_SIGHT;
        }
    }


    // Use this for initialization
    void Start () {
        // tx = transform;
        // x =
	}

	// Update is called once per frame
	protected override void Update () {
        if (MMGame.instance.IsChanging())
            return;
        base.Update();

        bool newMoving = false;
        if (!isMoving) {
            // Check next position
            switch (mode) {
                case MonsterMode.ATTACK: {
                    float f = Mathf.Min(1, (Time.time - modeStartTime) / 0.1f);
                    LookAt(modeTarget);
                    if (!FollowPath(false) && f == 1 && !modeTarget.isMoving) {
                        // Player?
                        if (heart.heartType == 3 && modeTarget is MMPlayer) {
                            MMPlayer player = MMGame.instance.player;

                            GameObject heartGO = GameObject.Instantiate<GameObject>(MMGame.instance.heartPrefab);
                            heartGO.transform.parent = transform.parent;
                            MMHeart playerHeart = heartGO.GetComponent<MMHeart>();
                            playerHeart.heartType = heartType;
                            playerHeart.monster = player;
                            playerHeart.SetMode(MMHeart.HeartMode.APPEARING, heart, Vector2Int.zero);

                            heart.SetMode(MMHeart.HeartMode.MELTED, playerHeart, Vector2Int.zero);
                            mode = MonsterMode.FROZEN;
                        } else {
                            heart.SetMode(MMHeart.HeartMode.DESTROYED, modeTarget is MMMonster ? ((MMMonster)modeTarget).heart : null, Vector2Int.zero);
                            mode = MonsterMode.FROZEN;
                        }
                        // Debug.Log("- target is now at: " + modeTarget.position);
                    }
                    break;
                }

                case MonsterMode.IN_LOVE: {
                    float f = Mathf.Min(1, (Time.time - modeStartTime) / 0.1f);
                    LookAt(modeTarget);
                    if (!FollowPath(false) && f == 1 && !modeTarget.isMoving) {
                        // Detach heart
                        heart.SetMode(MMHeart.HeartMode.MELTED, modeTarget is MMMonster ? ((MMMonster)modeTarget).heart : null, Vector2Int.zero);
                        mode = MonsterMode.FROZEN;
                    }
                    break;
                }

                case MonsterMode.LOST_SIGHT: {
                    if (MMGame.instance.IsMonsterSpecialRunning())
                        break;
                    // Can see player?
                    if (!TryToCatchMonster() && !TryToCatchPlayer()) {
                        if (Time.time - modeStartTime > 1) {
                            modeStartTime = Time.time;
                            if (++lostSightRotation > 3) {
                                mode = MonsterMode.PATROL;

                                MMGame.instance.PlaySound(MMGame.instance.soundGaveUp, this, UnityEngine.Random.Range(0.9f, 1.2f));

                                // Navigate back home
                                //-kill stack
                                while (chaseStack.Count > 2)
                                    chaseStack.Pop();
                                //-get direction
                                Vector2Int homePos = chaseStack.Pop();
                                //-get path
                                List<Vector2Int> path = Pathfinder.FindPath(MMGame.instance, this, null, homePos.x, homePos.y, this.x, this.y);
                                if (path != null && path.Count > 0) {
                                    foreach (Vector2Int pos in path)
                                        chaseStack.Push(pos);
                                }
                            } else {
                                direction = direction + 1;
                            }
                        }
                    }

                    break;
                }

                case MonsterMode.CHASING: {
                    if (MMGame.instance.IsMonsterSpecialRunning())
                        break;
                    // Update path
                    if (!TryToCatchMonster())
                        TryToCatchPlayer();

                    // Follow path
                    if (FollowPath(true))
                        break;
                    if (!NavigateToPoint(currentPOI.x, currentPOI.y, null, true)) {
                        MMGame.instance.PlaySound(MMGame.instance.soundLostSight, this, UnityEngine.Random.Range(0.9f, 1.1f), 0.35f, 0.5f);
                        mode = MonsterMode.LOST_SIGHT;
                        lostSightRotation = 0;
                        modeStartTime = Time.time;
                    }

                    break;
                }

                case MonsterMode.PATROL: {
                    if (MMGame.instance.IsMonsterSpecialRunning())
                        break;
                    // check if we caught the player
                    if (TryToCatchMonster() || TryToCatchPlayer())
                        break;

                    // returning home?
                    if (chaseStack.Count > 0) {
                        Vector2Int nextStep = chaseStack.Pop();
                        if (chaseStack.Count == 0) {
                            // Last one was the direction
                            direction = DirectionByVector(nextStep);
                        } else {
                            // Occupied? Then wait
                            direction = DirectionByVector(nextStep - position);
                            if (!MMGame.instance.TileOccupied(nextStep.x, nextStep.y, -1, this)) {
                                MoveTo(nextStep.x, nextStep.y);
                            } else {
                                // Put back for next frame
                                chaseStack.Push(nextStep);
                            }
                        }
                    } else {
                        // if not: walk forward
                        int dir = direction;
                        MMTileType type = MMGame.instance.Tile(x, y, direction);
                        if (type != MMTileType.MONSTERTRACK) {
                            dir = RotateDirection(direction, rightTurner ? 1 : -1);
                            type = MMGame.instance.Tile(x, y, dir);
                        }
                        if (type != MMTileType.MONSTERTRACK) {
                            dir = RotateDirection(direction, rightTurner ? -1 : 1);
                            type = MMGame.instance.Tile(x, y, dir);
                        }
                        if (type != MMTileType.MONSTERTRACK) {
                            dir = RotateDirection(direction, 2);
                            type = MMGame.instance.Tile(x, y, dir);
                        }
                        if (type == MMTileType.MONSTERTRACK) {
                            // Occupied? Then wait
                            if (!MMGame.instance.TileOccupied(x, y, dir, this)) {
                                MoveTowards(dir);
                            }
                        }
                    }

                    break;
                }
            }

            newMoving = isMoving;
        }

        if (newMoving)
            base.Update();
	}

    private bool FollowPath(bool noticeOthers) {
        if (pathToPOI.Count > 0) {
            Vector2Int nextStep = pathToPOI[0];
            MMTileObject obstacle = MMGame.instance.TileOccupied(nextStep.x, nextStep.y, -1, this);
            direction = DirectionByVector(nextStep.x - x, nextStep.y - y);
            if (obstacle == null) {
                MoveTo(nextStep.x, nextStep.y);
                pathToPOI.RemoveAt(0);
                // chaseStack.Push(nextStep);
                return true;
            } else if (noticeOthers && obstacle is MMPlayer) {
                NoticePlayer(obstacle);
                return true;
            } else if (noticeOthers && obstacle is MMMonster) {
                Encounter((MMMonster)obstacle);
                return true;
            }
        }

        // Direct notice of player
        if (noticeOthers && MMGame.instance.player.Distance(this) <= 1.5f) {
            NoticePlayer(MMGame.instance.player);
        }

        return false;
    }

    private void NoticePlayer(MMTileObject obstacle) {
        // End
        bool approachOk = NavigateToPoint(obstacle.target.x, obstacle.target.y, MMGame.instance.player, true);
        modeStartTime = Time.time;
        mode = MonsterMode.ATTACK;
        modeTarget = obstacle;
        // Debug.Log("Met player at " + obstacle.position + ", following him to " + obstacle.target + ": " + (approachOk ? "ok" : "failed"));
    }

    internal bool IsEncounterBlocked(MMMonster other) {
        return other.modeTarget != null && other.modeTarget != this;
        // foreach (MMMonster monster in MMGame.instance.monsters) {
        //     if (monster.mode == MMMonster.MonsterMode.ATTACK || monster.mode == MMMonster.MonsterMode.IN_LOVE || monster.mode == MMMonster.MonsterMode.FROZEN) {
        //         if (monster == other)
        //             return monster.modeTarget != this;
        //     }
        // }
        // return false;
    }

    private void Encounter(MMMonster other) {
        // Only one encounter
        if (IsEncounterBlocked(other))
            return;

        // Love or Hate?
        modeStartTime = Time.time;
        mode = other.heartType == heartType ? MonsterMode.IN_LOVE : MonsterMode.ATTACK;
        modeTarget = other;
        other.mode = mode;
        other.modeTarget = this;
        other.modeStartTime = modeStartTime;
        //other.direction = DirectionByVector(x - other.x, y - other.y);

        // Meet in the middle
        List<Vector2Int> path = Pathfinder.FindPath(MMGame.instance, this, other, targetX, targetY, other.targetX, other.targetY);
        Vector2Int meetingPoint = Vector2Int.zero;
        bool approachOk = false;
        if (path != null && path.Count > 0) {
            int m = Math.Max(1, path.Count / 2);
            meetingPoint = path[m];
            approachOk = NavigateToPoint(meetingPoint.x, meetingPoint.y, other, true);
            other.NavigateToPoint(meetingPoint.x, meetingPoint.y, this, true);
        }

        // Debug.Log("Met monster at " + other.position + ", meeting at " + meetingPoint + ": " + (approachOk ? "ok" : "failed"));
    }

    private bool TryToCatchPlayer() {
        return TryToCatch(MMGame.instance.player);
    }

    private bool TryToCatchMonster() {
        // Get closest monster
        MMMonster closest = null;
        float distance = 1000;
        foreach (MMMonster m in MMGame.instance.monsters) {
            if (m != this && m.Distance(this) < distance) {
                distance = m.Distance(this);
                closest = m;

                if (distance < 1.25f || (distance < 3f && CanSee(m))) {
                    Encounter(m);
                    return true;
                }
            }
        }

        return closest != null ? TryToCatch(closest) : false;
    }

    private bool TryToCatch(MMTileObject other) {
        // Can we see the player?
        if (CanSee(other)) {
            if (NavigateToPoint(other.x, other.y, other)) {
                mode = MonsterMode.CHASING;
                MMGame.instance.PlaySound(MMGame.instance.soundNotice, this, UnityEngine.Random.Range(0.9f, 1.2f), 0.2f, 0.1f);
                return true;
            }
        }

        return false;
    }

    public bool NavigateToPoint(int x, int y, MMTileObject target, bool forcePathRecalculation = false) {
        if (x == this.target.x && y == this.target.y && pathToPOI.Count == 0)
            return false;
        if (x == currentPOI.x && y == currentPOI.y && !forcePathRecalculation)
            return false;

        // Store current position to return to it
        if (chaseStack.Count == 0) {
            chaseStack.Push(DirectionVector());
            chaseStack.Push(position);
        }

        // Find path to target
        List<Vector2Int> path = Pathfinder.FindPath(MMGame.instance, this, target, this.target.x, this.target.y, x, y);
        if (path != null && path.Count > 0) {
            currentPOI = new Vector2Int(x, y);
            this.pathToPOI.Clear();
            this.pathToPOI.AddRange(path);
            this.pathToPOI.RemoveAt(0); // this is our current coordinate
            return true;
        }
        return false;
    }

    public bool CanSee(MMTileObject other) {
        float distance = other.Distance(this);
        if (distance <= (other is MMMonster ? VIEW_DISTANCE_MONSTER : VIEW_DISTANCE_PLAYER)) {
            int dx = other.x - x;
            int dy = other.y - y;
            int s = Math.Max(Mathf.Abs(dx), Mathf.Abs(dy));
            if (s == 0) {
                // You ded
                return false;
            }
            float sx = dx / s;
            float sy = dy / s;
            bool viewBlocked = false;
            for (int i=1; i<s && !viewBlocked; i++) {
                float tx = x + dx * ((float)i / s);
                float ty = y + dy * ((float)i / s);
                viewBlocked |= MMGame.instance.BlocksView(Mathf.RoundToInt(tx), Mathf.RoundToInt(ty));
                if (!viewBlocked && Mathf.RoundToInt(tx) != Mathf.RoundToInt(tx + 0.25f))
                    viewBlocked |= MMGame.instance.BlocksView(Mathf.RoundToInt(tx + 0.25f), Mathf.RoundToInt(ty));
                if (!viewBlocked && Mathf.RoundToInt(tx) != Mathf.RoundToInt(tx - 0.25f))
                    viewBlocked |= MMGame.instance.BlocksView(Mathf.RoundToInt(tx - 0.25f), Mathf.RoundToInt(ty));
                if (!viewBlocked && Mathf.RoundToInt(ty) != Mathf.RoundToInt(ty + 0.25f))
                    viewBlocked |= MMGame.instance.BlocksView(Mathf.RoundToInt(tx), Mathf.RoundToInt(ty + 0.25f));
                if (!viewBlocked && Mathf.RoundToInt(ty) != Mathf.RoundToInt(ty - 0.25f))
                    viewBlocked |= MMGame.instance.BlocksView(Mathf.RoundToInt(tx), Mathf.RoundToInt(ty - 0.25f));
            }
            if (!viewBlocked) {
                return true;
            }
        }
        return false;
    }
}
