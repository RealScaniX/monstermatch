﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MMPlayer : MMTileObject {
    private int moveDirection = -1;

    private float exitReached = -1;
    private bool makeNoise;
    private float lastNoiseTime = -1;

    // Use this for initialization
    void Start () {

	}

	// Update is called once per frame
	protected override void Update () {
        if (MMGame.instance.IsChanging())
            return;
        base.Update();

        bool newMoving = false;
        if (MMGame.instance.IsMonsterSpecialRunning()) {
            // Just wait
        } else if (exitReached > -1) {
            // Wait some, then go to next level
            float f = Mathf.Min(1, Mathf.Max(0, Time.time - exitReached - 1) / 1);
            Vector3 pos = tx.position;
            pos.y = -f * 1;
            tx.position = pos;
            if (f == 1) {
                // Play sound
                MMGame.instance.FadeOut(true);
            }
        } else {
            if (!isMoving) {
                if (MMGame.Key(MMGame.LEFT_KEYS)) {
                    moveDirection = 3;
                } else if (MMGame.Key(MMGame.UP_KEYS)) {
                    moveDirection = 0;
                } else if (MMGame.Key(MMGame.RIGHT_KEYS)) {
                    moveDirection = 1;
                } else if (MMGame.Key(MMGame.DOWN_KEYS)) {
                    moveDirection = 2;
                }
                if (MMGame.KeyDown(KeyCode.Space))
                    makeNoise = true;
                if (makeNoise && (Time.time - lastNoiseTime > 1)) {
                    makeNoise = false;
                    MMGame.instance.PlaySound(MMGame.instance.soundWhistle, this, Random.Range(0.8f, 1.3f), 0);
                    GameObject sw = Instantiate<GameObject>(MMGame.instance.soundWavePrefab);
                    sw.transform.position = new Vector3(tx.position.x, sw.transform.position.y, tx.position.z);
                    sw.GetComponent<SoundWave>().Begin();

                    // Alarm all monsters in that range
                    foreach (MMMonster m in MMGame.instance.monsters) {
                        if (m.Distance(this) <= 5) {
                            m.NavigateToPoint(x, y, MMGame.instance.player, true);
                            m.mode = MMMonster.MonsterMode.CHASING;
                            MMGame.instance.PlaySound(MMGame.instance.soundNotice, m, Random.Range(0.9f, 1.2f), 0.3f, 0.2f);
                        }
                    }
                    lastNoiseTime = Time.time;
                }
                if (moveDirection > -1) {
                    if (!MMGame.instance.TileOccupied(x, y, moveDirection, this)) {
                        MoveTowards(moveDirection);

                        // Reached exit?
                        MMTileObject obj = MMGame.instance.TileObjectAt(targetX, targetY);
                        if (obj is MMExit) {
                            if (((MMExit)obj).IsOpen()) {
                                if (MMGame.instance.IsMonsterChasing())
                                    MMGame.instance.SetMessage("I cannot leave while a monster is chasing me.");
                                else
                                    exitReached = Time.time;
                            } else {
                                MMGame.instance.SetMessage("I haven't completed this level. I should help those monsters.");
                            }
                        }
                    } else {
                        direction = moveDirection;
                    }
                    moveDirection = -1;
                }

                newMoving = isMoving;
            } else {
                if (MMGame.KeyDown(MMGame.LEFT_KEYS)) {
                    moveDirection = 3;
                } else if (MMGame.KeyDown(MMGame.UP_KEYS)) {
                    moveDirection = 0;
                } else if (MMGame.KeyDown(MMGame.RIGHT_KEYS)) {
                    moveDirection = 1;
                } else if (MMGame.KeyDown(MMGame.DOWN_KEYS)) {
                    moveDirection = 2;
                }
                if (MMGame.KeyDown(KeyCode.Space)) {
                    makeNoise = true;
                }
            }
        }

        if (newMoving)
            base.Update();
	}
}
