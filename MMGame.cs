﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Threading;
#if UNITY_EDITOR
using UnityEditor;
#endif
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class MMGame : MonoBehaviour {

    private const float QUIT_HOLD_TIME = 3f;

    public static readonly KeyCode[] LEFT_KEYS = {KeyCode.LeftArrow, KeyCode.A};
    public static readonly KeyCode[] UP_KEYS = {KeyCode.UpArrow, KeyCode.W};
    public static readonly KeyCode[] DOWN_KEYS = {KeyCode.DownArrow, KeyCode.S};

    public static readonly KeyCode[] RIGHT_KEYS = {KeyCode.RightArrow, KeyCode.D};

    public static Color COLOR_BLACK = new Color(0, 0, 0, 1);
    public static Color COLOR_BLACK_INVISIBLE = new Color(0, 0, 0, 0);

    [Header("Basics")]
    public MMLevel[] levels;

    [SerializeField]
    private float cameraBorder = 2f;

    [SerializeField]
    private int startLevel;

    [SerializeField]
    private Text messageText;

    [SerializeField]
    private Text levelText;

    [SerializeField]
    private RawImage infoBorder;

    [SerializeField]
    private RawImage menuImage;

    [SerializeField]
    private GameObject menuCanvas;

    [SerializeField]
    private Text menuText;

    [SerializeField]
    private RawImage fadePain;

    [Header("Audio")]
    public AudioClip soundMenuSelect;
    public AudioClip soundWhistle;
    public AudioClip soundExitOpened;
    public AudioClip soundNotice;
    public AudioClip soundLostSight;
    public AudioClip soundGaveUp;
    public AudioClip[] soundsLove;
    public AudioClip[] soundsAttack;

    [Header("Border")]
    public Texture borderAlert;
    public Texture borderLostSight;
    public Texture borderFinished;
    public Texture borderLove;
    public Texture borderHate;

    [Header("Parts")]
    public GameObject floor;
    public GameObject parts;

    public GameObject playerPrefab;
    public GameObject heartPrefab;
    public GameObject soundWavePrefab;
    public GameObject exitPrefab;
    public GameObject entrancePrefab;
    public GameObject doorPrefab;
    public GameObject wallPrefab;
    public GameObject monsterTrackPrefab;
    public GameObject monsterTrackInbetweenPrefab;

    public GameObject[] monsterPrefabs;

    public static MMGame instance;

    public static int levelNumber;

    public static bool[] visitedLevels = new bool[10];

    public static bool isWeb {
        get {
            return Application.platform == RuntimePlatform.WebGLPlayer;
        }
    }

    [HideInInspector]
    internal MMPlayer player;

    [HideInInspector]
    internal MMMonster[] monsters;

    [HideInInspector]
    internal MMDoor[] doors;


    public bool gameRunning;
    private int maxX;
    private int maxY;
    private Transform camTx;
    private float startTime;
    private float camY;
    private LightMode currentLightMode = LightMode.DEFAULT;
    private int pendingMessageDelay;
    private string pendingMessage;
    private bool camInitialized;
    private float fadeOutStart = -1;
    private bool fadeQuick;
    private bool fadeOutWon;

    private bool inMenu;
    private float escapeStart = -1;
    private string menuBaseText;
    private AudioSource audioSource;
    private int currentOption;
    private int optionCount;
    private static int brightnessLevel = -1;
    private static int musicVolume = 3;
    private static int soundVolume = 4;
    private float borderFade = 0f;

    public MMLevel currentLevel {
        get {
            return levels[levelNumber];
        }
    }

	void Awake() {
		instance = this;
        if (Application.isEditor)
            levelNumber = startLevel;

        menuBaseText = menuText.text;

        audioSource = GetComponent<AudioSource>();
	}

    void Start() {
        camTx = Camera.main.transform;
        camY = camTx.position.y;
        fadePain.gameObject.SetActive(true);
        fadePain.color = new Color(0, 0, 0, 1);

        parts.SetActive(false);
        floor.SetActive(true);

        foreach (GameObject o in new GameObject[]{
            playerPrefab,
            monsterPrefabs[0],
            monsterPrefabs[1],
            monsterPrefabs[2],
            monsterPrefabs[3],
            wallPrefab,
            entrancePrefab,
            exitPrefab,
            monsterTrackPrefab,
            monsterTrackInbetweenPrefab,
        }) {
            PrepareGameObject(o);
        }

        if (brightnessLevel == -1)
            brightnessLevel = isWeb ? 1 : 0;
        ApplyBrightness();
        ApplyMusicVolume();
        ApplySoundVolume();

        BuildLevel();

        SetInMenu(true);
    }

    public void PlaySound(AudioClip[] sounds, MMTileObject emitter, float pitch, float threeDNess = 0.35f, float delay = 0) {
        int idx = UnityEngine.Random.Range(0, sounds.Length);
        PlaySound(sounds[idx], emitter, pitch, threeDNess, delay);
    }

    public void PlaySound(AudioClip sound, MMTileObject emitter, float pitch, float threeDNess = 0.35f, float delay = 0) {
        float vol = soundVolume / 5f;
        OneShotSound.PlaySound(sound, emitter, threeDNess * 0, vol*vol, pitch, delay);
    }

    private void ApplyBrightness() {
        RenderSettings.ambientSkyColor = Color.Lerp(new Color(0.28f, 0.28f, 0.28f), new Color(0.5f, 0.5f, 0.5f), brightnessLevel / 5f);
    }

    private void ApplyMusicVolume() {
        if (audioSource != null)
            audioSource.volume = Mathf.Pow(musicVolume / 5f, 2);
    }

    private void ApplySoundVolume() {
    }

    private void PrepareGameObject(GameObject o) {
        MeshRenderer[] mrs = o.GetComponentsInChildren<MeshRenderer>();
        foreach (MeshRenderer mr in mrs) {
            foreach (Material mat in mr.sharedMaterials) {
                mat.SetFloat("_GlossyReflections", 0);
                mat.SetFloat("_SpecularHighlights", 1);
            }
        }
    }

    private void BuildLevel() {
        // Clean up old levels
        monsters = new MMMonster[0];
        doors = new MMDoor[0];
        foreach (MMLevel l in levels)
            l.CleanUp();

        // Build level
        currentLevel.Build();
        visitedLevels[levelNumber] = true;

        switch (levelNumber) {
            case 0:
                levelText.text = "I";
                break;
            case 1:
                levelText.text = "II";
                break;
            case 2:
                levelText.text = "III";
                break;
            case 3:
                levelText.text = "IV";
                break;
            case 4:
                levelText.text = "V";
                break;
            case 5:
                levelText.text = "VI";
                break;
            default:
                levelText.text = "";
                break;
        }

        // All ok?
        // if (player == null) {
        //     Debug.LogError("No player in level!");
        //     return;
        // }
        // if (player == null) {
        //     Debug.LogError("No player in level!");
        //     return;
        // }

        // Calculate bounds
        Transform txLevel = currentLevel.transform;
        maxX = 0;
        maxY = 0;
        for (int i=0; i<txLevel.childCount; i++) {
            Vector3 pos = txLevel.GetChild(i).transform.localPosition;
            maxX = Math.Max(maxX, Mathf.RoundToInt(pos.x));
            maxY = Math.Max(maxY, Mathf.RoundToInt(pos.z));
        }

        // Create inbetweens
        for (int x=0; x<maxX; x++) {
            for (int y=0; y<maxY; y++) {
                if (Tile(x, y) == MMTileType.MONSTERTRACK) {
                    if (Tile(x+1, y) == MMTileType.MONSTERTRACK) {
                        MMTileObject o = currentLevel.PlaceObject<MMTileObject>(monsterTrackInbetweenPrefab, x, y, 0 + UnityEngine.Random.Range(0, 2) * 2);
                        o.transform.localPosition = o.transform.localPosition + Vector3.right * 0.5f;
                    }
                    if (Tile(x, y+1) == MMTileType.MONSTERTRACK) {
                        MMTileObject o = currentLevel.PlaceObject<MMTileObject>(monsterTrackInbetweenPrefab, x, y, 1 + UnityEngine.Random.Range(0, 2) * 2);
                        o.transform.localPosition = o.transform.localPosition + Vector3.forward * 0.5f;
                    }
                }
            }
        }

        // Show level message
        MMGame.instance.SetMessage(currentLevel.message);

        camInitialized = false;
        startTime = Time.time;
        gameRunning = true;
    }

    public MMTileType Tile(int x, int y, int direction = -1) {
        Vector2Int dirVec = MMTileObject.DirectionVector(direction);
        x += dirVec.x;
        y += dirVec.y;
        char t = currentLevel.Tile(x, y, true);
        if (t == '#') {
            return MMTileType.WALL;
        } else if (t == '.' || t == ':' || (t >= '1' && t <= '9')) {
            return MMTileType.MONSTERTRACK;
        } else if (t == '*') {
            return MMTileType.ENTRANCE;
        } else if (t == '>') {
            return MMTileType.EXIT;
        }
        return MMTileType.EMPTY;
    }

    internal void RemoveMonster(MMTileObject monster) {
        Destroy(monster.gameObject);
        List<MMMonster> newlist = new List<MMMonster>();
        foreach (MMMonster m in monsters)
            if (m != monster) newlist.Add(m);
        monsters = newlist.ToArray();
    }

    public MMTileObject TileObjectAt(int x, int y) {
        Transform txLevel = currentLevel.transform;
        for (int i=0; i<txLevel.childCount; i++) {
            Vector3 pos = txLevel.GetChild(i).transform.localPosition;
            if (Mathf.RoundToInt(pos.x) == x && Mathf.RoundToInt(pos.z) == y)
                return txLevel.GetChild(i).GetComponent<MMTileObject>();
        }
        return null;
    }

    public bool BlocksView(int x, int y) {
        MMTileObject obj = MMGame.instance.TileOccupied(x, y);
        return (obj is MMWall || obj is MMDoor);
    }

    public MMTileObject TileOccupied(int x, int y, int direction = -1, MMTileObject ignored = null) {
        Vector2Int dirVec = MMTileObject.DirectionVector(direction);
        x += dirVec.x;
        y += dirVec.y;

        char t = currentLevel.Tile(x, y);
        if (t == '#')
            return TileObjectAt(x, y);

        foreach (MMMonster monster in monsters) {
            if (monster != ignored && monster.Blocks(x, y))
                return monster;
        }

        if (player != ignored && player.Blocks(x, y))
            return player;

        foreach (MMDoor door in doors) {
            if (door != ignored && door.Blocks(x, y))
                return door;
        }

        return null;
    }

    internal bool IsMonsterSpecialRunning() {
        foreach (MMMonster monster in monsters) {
            if (monster.mode == MMMonster.MonsterMode.ATTACK || monster.mode == MMMonster.MonsterMode.IN_LOVE || monster.mode == MMMonster.MonsterMode.FROZEN)
                return true;
        }
        return false;
    }

    internal bool IsMonsterChasing() {
        foreach (MMMonster monster in monsters) {
            if (monster.mode == MMMonster.MonsterMode.CHASING)
                return monster;
        }
        return false;
    }

    public bool IsInMenu() {
        return inMenu;
    }

    private void SetInMenu(bool v) {
        Input.ResetInputAxes();
        inMenu = v;
        escapeStart = -1;
        menuCanvas.SetActive(v);
        Time.timeScale = v ? 0 : 1;
        currentOption = 1;
        menuImage.color = Color.white;
        messageText.gameObject.SetActive(!v);
        levelText.gameObject.SetActive(!v);
        infoBorder.enabled = !v;
        if (v)
            UpdateMenuText(true);
    }

    private void UpdateMenuText(bool silent = false) {
        string menuText = this.menuBaseText;

        if (!silent)
            PlaySound(soundMenuSelect, null, UnityEngine.Random.Range(0.9f, 1.1f), 0, 0);

        optionCount = 8;
        menuText += "\n";
        for (int i=1; i<6; i++)
            if (visitedLevels[i]) {
                optionCount++;
                menuText += ((i-1) % 2 == 0 ? "\n" : "    ") + "{" + (optionCount-1) + "} Load level " + (i+1);
            }

        for (int i=0; i<optionCount; i++)
            menuText = menuText.Replace("{" + i + "}", currentOption == i ? ">" : " ");

        menuText = menuText.Replace("{BR}", BarStr(brightnessLevel));
        menuText = menuText.Replace("{SND}", BarStr(soundVolume));
        menuText = menuText.Replace("{MUS}", BarStr(musicVolume));

        this.menuText.text = menuText;
    }

    private string BarStr(int level) {
        string s = "";
        for (int i=0; i<level; i++)
            s += "=";
        while (s.Length < 5)
            s += "-";
        return s;
    }

    private void FailedLevel() {
        BuildLevel();
    }

    private void NextLevel() {
        if (levelNumber < 6) { // hard coded, test levels follow the normal ones
            levelNumber++;
            BuildLevel();
        } else {
            SceneManager.LoadScene("ending");
        }
    }

    public void FadeOut(bool won, bool quick = false) {
        if (MMGame.instance.IsChanging())
            return;
        fadeOutStart = Time.time;
        fadeQuick = quick;
        fadeOutWon = won;
    }

    public bool IsChanging() {
        return IsInMenu() || fadeOutStart > -1;
    }

	void Update() {
        if (IsInMenu()) {
            ProcessMenuKeys();
            return;
        }

        // Message
        if (pendingMessageDelay > 0) {
            if (--pendingMessageDelay == 0) {
                messageText.text = pendingMessage;
            }
        }

        // cheat
        if (Input.GetKey(KeyCode.LeftAlt) && Input.GetKey(KeyCode.LeftControl) && Input.GetKey(KeyCode.LeftShift)) {
            if (Input.GetKeyDown(KeyCode.Alpha1)) {
                levelNumber = 0;
                BuildLevel();
            } else if (Input.GetKeyDown(KeyCode.Alpha2)) {
                levelNumber = 1;
                BuildLevel();
            } else if (Input.GetKeyDown(KeyCode.Alpha3)) {
                levelNumber = 2;
                BuildLevel();
            } else if (Input.GetKeyDown(KeyCode.Alpha4)) {
                levelNumber = 3;
                BuildLevel();
            } else if (Input.GetKeyDown(KeyCode.Alpha5)) {
                levelNumber = 4;
                BuildLevel();
            } else if (Input.GetKeyDown(KeyCode.Alpha6)) {
                levelNumber = 5;
                BuildLevel();
            }
        } else if (Input.GetKeyDown(KeyCode.Escape)) {
            SetInMenu(true);
            return;
        } else if (Input.GetKeyDown(KeyCode.Q)) {
            FadeOut(false, true);
        }

        // Fade in
        float t = Time.time - startTime;
        if (fadeOutStart > -1) {
            float f2 = Mathf.Min(1, (Time.time - fadeOutStart) / (fadeQuick ? 0.4f : 1.5f));
            if (fadePain.gameObject.activeSelf != f2 > 0)
                fadePain.gameObject.SetActive(f2 > 0);
            fadePain.color = Color.Lerp(COLOR_BLACK, COLOR_BLACK_INVISIBLE, Mathf.SmoothStep(0, 1, 1-f2));
            if (f2 == 1) {
                fadeOutStart = -1;
                if (fadeOutWon)
                    NextLevel();
                else
                    FailedLevel();
            }
        } else {
            float f2 = Mathf.Min(1, t / 1.5f);
            if (f2 < 1 || !fadePain.gameObject.activeSelf) {
                if (fadePain.gameObject.activeSelf != f2 < 1)
                    fadePain.gameObject.SetActive(f2 < 1);
                fadePain.color = Color.Lerp(COLOR_BLACK, COLOR_BLACK_INVISIBLE, Mathf.SmoothStep(0, 1, f2));
            }
        }

        // Level text
        if (t < 4) {
            float f = Mathf.Max(0, Mathf.Min(t / 0.75f, (3-t)/ 1.5f));
            if (f > 0 != levelText.gameObject.activeSelf)
                levelText.gameObject.SetActive(f > 0);
            levelText.color = Color.Lerp(new Color(0.5f, 0.1f, 0.8f, 0), messageText.color, Mathf.SmoothStep(0, 1, f));
        }

        // Move camera
        Vector3 targetPosOverview = new Vector3(maxX/2f, 15, maxY/2f - 6);
        if (!camInitialized) {
            camTx.localPosition = targetPosOverview;
            camInitialized = true;
        } else if (player != null) {
            MMTileObject focus = player;
            foreach (MMMonster m in monsters) {
                if (m.heart != null && m.heart.triggered && m.Distance(player) > 5)
                    focus = m;
            }
            Vector3 targetPosPlayer = focus.transform.position;
            targetPosPlayer.y = camY;
            targetPosPlayer.x = Mathf.Max(cameraBorder, Mathf.Min(targetPosPlayer.x, maxX - cameraBorder));
            targetPosPlayer.z = Mathf.Max(cameraBorder, Mathf.Min(targetPosPlayer.z, maxY - cameraBorder)) - 3;

            float f = Mathf.Min(1, (Time.time - startTime) / (levelNumber < 5 ? 15 : 7));
            Vector3 targetPos = Vector3.Lerp(targetPosOverview, targetPosPlayer, Mathf.SmoothStep(0, 1, f));

            Vector3 currentPos = camTx.localPosition;
            camTx.localPosition = Vector3.MoveTowards(currentPos, targetPos, Mathf.Min(12, 2*(targetPosPlayer - currentPos).magnitude) * Time.deltaTime);
        }

        // Can any monster see the player?
        LightMode lightMode = LightMode.DEFAULT;
        if (!IsChanging()) {
            lightMode = monsters.Length == 1 ? LightMode.FINISHED : LightMode.DEFAULT;
            foreach (MMMonster m in monsters) {
                if (m.mode == MMMonster.MonsterMode.CHASING) {
                    lightMode = LightMode.PANIC;
                    break;
                } else if (m.mode == MMMonster.MonsterMode.LOST_SIGHT) {
                    if (lightMode == LightMode.DEFAULT || lightMode == LightMode.FINISHED)
                        lightMode = LightMode.LOST_SIGHT;
                } else if (m.heart != null && m.heart.mode == MMHeart.HeartMode.MELTED) {
                    if (m.heartType == 3) {
                        if (m.heart.other != null && m.heart.other.mode == MMHeart.HeartMode.MELTED) {
                            lightMode = LightMode.LOVE;
                        } else {
                            lightMode = LightMode.HATE;
                        }
                    } else {
                        lightMode = LightMode.LOVE;
                    }
                } else if (m.heart != null && m.heart.mode == MMHeart.HeartMode.DESTROYED) {
                    if (lightMode != LightMode.LOVE)
                        lightMode = LightMode.HATE;
                }
            }
        }
        if (lightMode != currentLightMode) {
            if (borderFade > 0) {
                borderFade = Mathf.Max(borderFade - Time.deltaTime * 4f, 0);
            } else {
                currentLightMode = lightMode;
                bool enabled = false;
                Texture texture = null;
                switch (lightMode) {
                    case LightMode.PANIC:
                        enabled = true;
                        texture = borderAlert;
                        break;
                    case LightMode.LOST_SIGHT:
                        enabled = true;
                        texture = borderLostSight;
                        break;
                    case LightMode.LOVE:
                        enabled = true;
                        texture = borderLove;
                        break;
                    case LightMode.HATE:
                        enabled = true;
                        texture = borderHate;
                        break;
                    case LightMode.FINISHED:
                        enabled = true;
                        texture = borderFinished;
                        break;
                }
                if (enabled != infoBorder.gameObject.activeSelf)
                    infoBorder.gameObject.SetActive(enabled);
                infoBorder.texture = texture;
            }
        } else {
            borderFade = Mathf.Min(borderFade + Time.deltaTime * 4f, 1);
        }
        Color color = new Color(1, 1, 1, borderFade * 0.15f);
        infoBorder.color = color;

        // Last level?
        if (!IsChanging() && player == null && Time.time - startTime > 10) {
            FadeOut(true);
        }

        // Screenshot
        if (Input.GetKeyDown(KeyCode.P)) {
            string workingDir = Directory.GetCurrentDirectory();
            DateTime dt = DateTime.Now;
            string filename = workingDir + "/" + "snap_" + string.Format("{0:D4}{1:D2}{2:D2}_{3:D2}{4:D2}{5:D2}", dt.Year, dt.Month, dt.Day, dt.Hour, dt.Minute, dt.Second) + ".png";
            ScreenCapture.CaptureScreenshot(filename);
            SetMessage("Screenshot saved to:\n" + filename);
        }
	}

    public void ProcessMenuKeys() {
        if (escapeStart > -1) {
            float f = Mathf.Min((Time.unscaledTime - escapeStart) / QUIT_HOLD_TIME, 1);
            menuImage.color = Color.Lerp(Color.white, Color.black, f);
            if (f == 1) {
                Quit();
            }
        }

        if (Input.GetKeyDown(KeyCode.Escape)) {
            escapeStart = Time.unscaledTime;
        } else if (Input.GetKeyUp(KeyCode.Escape)) {
            if (escapeStart > -1 && Time.unscaledTime - escapeStart > QUIT_HOLD_TIME) {
                Quit();
            } else if (Time.unscaledTime - escapeStart < 0.5f) {
                SetInMenu(false);
            }
            menuImage.color = Color.white;
            escapeStart = -1;
        } else if (KeyDown(DOWN_KEYS)) {
            currentOption = (currentOption + 1) % optionCount;
            UpdateMenuText();
        } else if (KeyDown(UP_KEYS)) {
            currentOption = (currentOption + (optionCount-1)) % optionCount;
            UpdateMenuText();
        } else if (KeyDown(LEFT_KEYS) || KeyDown(RIGHT_KEYS)) {
            int d = KeyDown(LEFT_KEYS) ? -1 : 1;
            switch (currentOption) {
                case 2: {
                    // Brightness
                    brightnessLevel = Mathf.Max(0, Math.Min(5, brightnessLevel + d));
                    ApplyBrightness();
                    UpdateMenuText();
                    break;
                }
                case 3: {
                    // Sound volume
                    soundVolume = Mathf.Max(0, Math.Min(5, soundVolume + d));
                    ApplySoundVolume();
                    UpdateMenuText();
                    break;
                }
                case 4: {
                    // music volume
                    musicVolume = Mathf.Max(0, Math.Min(5, musicVolume + d));
                    ApplyMusicVolume();
                    UpdateMenuText();
                    break;
                }
            }
        } else if (KeyDown(KeyCode.KeypadEnter, KeyCode.Return)) {
            switch (currentOption) {
                case 0: {
                    // new game
                    levelNumber = 0;
                    BuildLevel();
                    SetInMenu(false);
                    break;
                }
                case 1: {
                    // continue
                    SetInMenu(false);
                    break;
                }
                case 2: {
                    // Brightness
                    brightnessLevel = (brightnessLevel + 1) % 6;
                    ApplyBrightness();
                    UpdateMenuText();
                    break;
                }
                case 3: {
                    // Sound
                    soundVolume = (soundVolume + 1) % 6;
                    ApplySoundVolume();
                    UpdateMenuText();
                    break;
                }
                case 4: {
                    // music
                    musicVolume = (musicVolume + 1) % 6;
                    ApplyMusicVolume();
                    UpdateMenuText();
                    break;
                }
                case 5: {
                    // Eeve Somepx
                    MMGame.OpenLink("https://twitter.com/somepx");
                    break;
                }
                case 6: {
                    // kevin
                    MMGame.OpenLink("https://incompetech.com");
                    break;
                }
                case 7: {
                    // scanix
                    MMGame.OpenLink("https://twitter.com/HiBeGame");
                    break;
                }
                default: {
                    // load levels
                    int k = (currentOption - 7);
                    levelNumber = 0;
                    for (int i=1; i<visitedLevels.Length && k > 0; i++) { // ignore level 0 as it is not an extra option below
                        if (visitedLevels[i]) {
                            levelNumber = i;
                            k--;
                        }
                    }
                    BuildLevel();
                    SetInMenu(false);
                    break;
                }
            }
        }
    }

    private void Quit() {
#if UNITY_EDITOR
        if (Application.isEditor)
            EditorApplication.isPlaying = false;
        else
#endif
            Application.Quit();
    }

    public void SetMessage(string text) {
        pendingMessageDelay = 10;
        pendingMessage = text;
    }

    public static bool Key(params KeyCode[] codes) {
        foreach (KeyCode code in codes)
            if (Input.GetKey(code))
                return true;
        return false;
    }

    public static bool KeyDown(params KeyCode[] codes) {
        foreach (KeyCode code in codes)
            if (Input.GetKeyDown(code))
                return true;
        return false;
    }

    public static bool KeyUp(params KeyCode[] codes) {
        foreach (KeyCode code in codes)
            if (Input.GetKeyUp(code))
                return true;
        return false;
    }

    internal static void OpenLink(string url) {
        if (isWeb) {
            Application.ExternalEval("window.open(\""+url+"\")");
        } else {
            Application.OpenURL(url);
        }
    }
}
