﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Rendering;

public class MMHeart : MMTileObject {

    public enum HeartMode {
        BROKEN,
        MELTED,
        APPEARING,
        DESTROYED,
        NONE
    }

    [SerializeField]
    private Color[] typeColors = {Color.blue, Color.green, Color.yellow};

    [SerializeField]
    private Material heartMaterial;

    [SerializeField]
    private float heartSplit = 0.075f;

    [SerializeField]
    private float hoverHeight = 0.75f;

    private int heartType_;

    internal MMTileObject monster;
    internal HeartMode mode;
    internal MMHeart other;
    private Vector2Int rendevouszPoint;
    private float startTime;
    private float phaseStartTime;
    private int modePhase;

    private Material centerMaterial;
    private Material borderMaterial;
    private Transform camTx;
    private Transform txRotator;
    private Transform txLeft;
    private Transform txRight;
    private Transform txFull;
    private Transform txCrack;
    private Quaternion startRotation;
    private Quaternion endRotation;
    private MeshRenderer leftMR;
    private MeshRenderer rightMR;
    private MeshRenderer fullMR;
    private MaterialPropertyBlock mpb;
    private bool frontReached;
    private bool leftSide;
    private Vector3 floatingStart;
    private Vector3 floatingTarget;
    private int syncCount_;
    private bool synched_;

    private bool synched {
        get {
            if (other != null && leftSide == other.leftSide)
                leftSide = LeftSide(other);
            if (other == null || !leftSide)
                syncCount_++;
            this.synched_ |= (other == null || !leftSide ? syncCount_ : other.syncCount_) > 3;
            if (other != null)
                other.synched_ |= this.synched_;
            return synched_;
        }
    }

    public int heartType {
        get {
            return heartType_;
        }
        set {
            heartType_ = value;
            if (leftMR != null)
                ApplyColor();
        }
    }

    private float phaseTime {
        get {
            return Time.time - phaseStartTime;
        }
    }

    public bool triggered {
        get {
            return mode != HeartMode.NONE && mode != HeartMode.BROKEN;
        }
    }

    public void SetMode(HeartMode mode, MMHeart other, Vector2Int rendevouszPoint) {
        this.mode = mode;
        this.other = other;
        this.rendevouszPoint = rendevouszPoint;
        syncCount_ = 0;
        phaseStartTime = Time.time;
        modePhase = 0;
        if (other != null)
            leftSide = LeftSide(other);

    }

    private void ApplyFade(float left, float right, float full) {
        if (left >= 0)
            FadeMaterial(leftMR, left);
        if (right >= 0)
            FadeMaterial(rightMR, right);
        if (full >= 0)
            FadeMaterial(fullMR, full);
    }

    private void FadeMaterial(MeshRenderer mr, float fade) {
        mpb.SetColor("_Color", Color.Lerp(typeColors[heartType], new Color(0, 0, 0, fade), 1 - fade * 0.5f));
        if (mr == fullMR)
            mpb.SetColor("_EmissionColor", Color.Lerp(typeColors[heartType], new Color(0, 0, 0, fade), 0.75f));
        else
            mpb.SetColor("_EmissionColor", Color.Lerp(typeColors[heartType], new Color(0, 0, 0, fade), 1 - fade * 0.25f));
        mr.SetPropertyBlock(mpb, 0);
        mpb.SetColor("_Color", Color.Lerp(typeColors[heartType], new Color(0, 0, 0, fade), 1 - fade));
        if (mr == fullMR)
            mpb.SetColor("_EmissionColor", Color.Lerp(typeColors[heartType], new Color(0, 0, 0, fade), 0.5f));
        else
            mpb.SetColor("_EmissionColor", Color.Lerp(typeColors[heartType], new Color(0, 0, 0, fade), 1 - fade * 0.5f));
        mr.SetPropertyBlock(mpb, 1);
    }

    private void ApplyColor() {
        if (centerMaterial == null) {
            centerMaterial = new Material(heartMaterial);
            borderMaterial = new Material(heartMaterial);
            centerMaterial.color = Color.Lerp(typeColors[heartType], Color.black, 0.5f);
            centerMaterial.SetFloat("_Glossiness", 0.6f);
            centerMaterial.SetColor("_EmissionColor", Color.Lerp(typeColors[heartType], Color.black, 0.75f));
            borderMaterial.color = typeColors[heartType];
            borderMaterial.SetFloat("_Glossiness", 0.1f);
            borderMaterial.SetColor("_EmissionColor", Color.Lerp(typeColors[heartType], Color.black, 0.5f));
        }
        MeshRenderer[] mrs = GetComponentsInChildren<MeshRenderer>();
        foreach (MeshRenderer mr in new MeshRenderer[]{leftMR, rightMR, fullMR}) {
            mr.sharedMaterials = new Material[]{centerMaterial, borderMaterial};
            mr.shadowCastingMode = ShadowCastingMode.Off;
        }
    }

    void OnDestroy() {
        if (centerMaterial != null)
            Destroy(centerMaterial);
    }

    protected override void Awake() {
        base.Awake();

        mpb = new MaterialPropertyBlock();
        SetMode(HeartMode.BROKEN, null, Vector2Int.zero);
    }

    void Start() {
        // int idx = 0;
        // foreach (MMMonster m in MMGame.instance.monsters) {
        //     if (m == monster) {
        //         leftPiece = idx % 2 == 0;
        //     }
        //     if (m.heartType == heartType) {
        //         idx++;
        //     }
        // }

        camTx = Camera.main.transform;
        txRotator = tx.GetChild(0);
        txLeft = txRotator.GetChild(0);
        txRight = txRotator.GetChild(1);
        txFull = txRotator.GetChild(2);
        txCrack = txRotator.GetChild(3);
        txLeft.gameObject.SetActive(true);
        txRight.gameObject.SetActive(true);
        txFull.gameObject.SetActive(false);
        txLeft.localPosition = new Vector3(-heartSplit, 0, 0);
        txRight.localPosition = new Vector3(heartSplit, 0, 0);
        leftMR = txLeft.gameObject.GetComponent<MeshRenderer>();
        rightMR = txRight.gameObject.GetComponent<MeshRenderer>();
        fullMR = txFull.gameObject.GetComponent<MeshRenderer>();
        txCrack.gameObject.SetActive(false);

        ApplyColor();
        switch (mode) {
            case HeartMode.APPEARING:
                ApplyFade(0, 0, 1);
                break;
            default:
                ApplyFade(1, 1, 1);
                break;
        }
        FollowMonster();

        startTime = Time.time + UnityEngine.Random.Range(0f, 3f);
    }

    public bool LeftSide(MMHeart other) {
        if (Mathf.Abs(other.tx.position.x - tx.position.x) > 0.1f)
            return other.tx.position.x > tx.position.x;
        foreach (MMMonster m in MMGame.instance.monsters) {
            if (m == monster)
                return true;
            if (m == other.monster)
                return false;
        }
        return false;
    }

    void LateUpdate() {
        tx.localEulerAngles = camTx.localEulerAngles / 2;

        // Crack
        bool shouldBeEnabled = monster != null && monster is MMMonster && (((MMMonster)monster).triggered);
        if (shouldBeEnabled != txCrack.gameObject.activeSelf)
            txCrack.gameObject.SetActive(shouldBeEnabled);

        switch (mode) {
            case HeartMode.BROKEN: {
                // Follow monster
                FollowMonster();
                break;
            }

            case HeartMode.DESTROYED: {
                if (!leftSide && other != null) {
                    modePhase = other.modePhase;
                    phaseStartTime = other.phaseStartTime;
                }

                switch (modePhase) {
                    case 0: {
                        MMGame.instance.PlaySound(MMGame.instance.soundsAttack, monster, UnityEngine.Random.Range(0.9f, 1.1f), 0.25f, leftSide ? 0 : 0.2f);
                        AdvancePhase();
                        break;
                    }

                    case 1: {
                        FollowMonster(true);
                        if (frontReached && (other == null || other.frontReached) && synched) {
                            floatingStart = tx.position;
                            if (other != null) {
                                other.floatingStart = other.tx.position;
                                floatingTarget = (other.tx.position + tx.position) / 2;
                                other.floatingTarget = floatingTarget;
                            } else {
                                floatingTarget = floatingStart;
                            }
                            AdvancePhase();
                        }
                        break;
                    }

                    case 2: {
                        // Fade out
                        FollowMonster(true);
                        if (other == null) {
                            AdvancePhase();
                        } else {
                            float f = Mathf.Min(phaseTime / .75f, 1);
                            ApplyFade(1-f, -1, -1);
                            if (f == 1 && synched)
                                AdvancePhase();
                        }
                        break;
                    }

                    case 3: {
                        // Meet
                        if (other == null) {
                            AdvancePhase();
                        } else {
                            float f = Mathf.Min(phaseTime / 0.6f, 1);
                            tx.position = Vector3.Lerp(floatingStart, floatingTarget + (leftSide ? -1 : 1) * 4 * heartSplit * Vector3.right, Mathf.SmoothStep(0, 1, f));
                            if (f == 1 && synched)
                                AdvancePhase();
                        }
                        break;
                    }

                    case 4: {
                        // Try to match
                        if (other == null) {
                            AdvancePhase();
                        } else {
                            float f = Mathf.Min(phaseTime / 0.6f, 1);
                            tx.position = Vector3.Lerp(floatingTarget + (leftSide ? -1 : 1) * 4 * heartSplit * Vector3.right, floatingTarget + (leftSide ? -1 : 1) * heartSplit * Vector3.right, Mathf.Pow(f, 1.75f));
                            txLeft.localPosition = new Vector3(-heartSplit * (1-f), 0, 0);
                            txRight.localPosition = new Vector3(heartSplit * (1-f), 0, 0);
                            if (f == 1 && synched) {
                                tx.position = floatingTarget;
                                AdvancePhase();
                            }
                        }
                        break;
                    }

                    case 5: {
                        // Crash (fall down)
                        if (other == null) {
                            float f = Mathf.Min(phaseTime / 1.5f, 1);
                            txLeft.position = Vector3.Lerp(floatingTarget + -heartSplit * Vector3.right, floatingTarget + -heartSplit * 8 * Vector3.right + Vector3.down * Mathf.Pow(f, 2) * 2, Mathf.Pow(f, 1.5f));
                            txRight.position = Vector3.Lerp(floatingTarget + heartSplit * Vector3.right, floatingTarget + heartSplit * 8 * Vector3.right + Vector3.down * Mathf.Pow(f, 2) * 2, Mathf.Pow(f, 1.5f));
                            txLeft.localEulerAngles = new Vector3(0, 0, f * 25f);
                            txRight.localEulerAngles = new Vector3(0, 0, -f * 25f);
                            ApplyFade(1-f, 1-f, -1);
                            if (f == 1 && synched) {
                                AdvancePhase();
                            }
                        } else {
                            float f = Mathf.Min(phaseTime / 0.6f, 1);
                            tx.position = Vector3.Lerp(floatingTarget + (leftSide ? -1 : 1) * heartSplit * Vector3.right, floatingTarget + (leftSide ? -1 : 1) * 8 * heartSplit * Vector3.right + Vector3.down * Mathf.Pow(f, 2) * 2, Mathf.Pow(f, 1.5f));
                            txRight.localEulerAngles = new Vector3(0, 0, -f * 35f);
                            txLeft.localEulerAngles = new Vector3(0, 0, -f * 35f);
                            ApplyFade(-1, 1-f, -1);
                            if (f == 1 && synched) {
                                tx.position = floatingTarget;
                                AdvancePhase();
                            }
                        }
                        break;
                    }

                    case 6: {
                        // Shockwave
                        float f = Mathf.Min(phaseTime / 0.5f, 1);

                        if (f == 1 && synched)
                            AdvancePhase();
                        break;
                    }

                    default: {
                        // Game over
                        MMGame.instance.FadeOut(false);
                        mode = HeartMode.NONE;
                        break;
                    }
                }

                break;
            }

            case HeartMode.APPEARING: {
                switch (modePhase) {
                    case 0: {
                        MMGame.instance.PlaySound(MMGame.instance.soundLostSight, monster, UnityEngine.Random.Range(0.9f, 1.1f));
                        AdvancePhase();
                        break;
                    }

                    case 1: {
                        // Fade in
                        float f = Mathf.Min(1, phaseTime / 3.0f);
                        FollowMonster(f > 0.5f);
                        ApplyFade(f, f, -1);
                        if (f == 1) {
                            SetMode(HeartMode.MELTED, other, rendevouszPoint);
                        }
                        break;
                    }
                }
                break;
            }

            case HeartMode.MELTED: {
                if (!leftSide) {
                    modePhase = other.modePhase;
                    phaseStartTime = other.phaseStartTime;
                }

                switch (modePhase) {
                    case 0: {
                        // Wait for other
                        FollowMonster(leftSide);
                        if ((other == null || other.mode != HeartMode.APPEARING) && synched) {
                            AdvancePhase();

                            MMGame.instance.PlaySound(MMGame.instance.soundsLove, monster, UnityEngine.Random.Range(0.9f, 1.1f), 0.35f, leftSide ? 0 : 0.2f);
                        }
                        break;
                    }

                    case 1: {
                        FollowMonster(true);
                        if (frontReached && other.frontReached && synched) {
                            floatingStart = tx.position;
                            other.floatingStart = other.tx.position;
                            floatingTarget = (other.tx.position + tx.position) / 2;
                            other.floatingTarget = floatingTarget;
                            AdvancePhase();
                        }
                        break;
                    }

                    case 2: {
                        // Fade out
                        if (!other.frontReached)
                            Debug.Break();
                        FollowMonster(true);
                        float f = Mathf.Min(phaseTime / .75f, 1);
                        ApplyFade(leftSide ? 1 : 1-f, leftSide ? 1-f : 1, -1);
                        if (f == 1 && synched)
                            AdvancePhase();
                        break;
                    }

                    case 3: {
                        // Meet
                        float f = Mathf.Min(phaseTime / 0.6f, 1);
                        tx.position = Vector3.Lerp(floatingStart, floatingTarget + (leftSide ? -1 : 1) * 4 * heartSplit * Vector3.right, Mathf.SmoothStep(0, 1, f));
                        if (f == 1 && synched)
                            AdvancePhase();
                        break;
                    }

                    case 4: {
                        // Combine
                        float f = Mathf.Min(phaseTime / 0.6f, 1);
                        tx.position = Vector3.Lerp(floatingTarget + (leftSide ? -1 : 1) * 4 * heartSplit * Vector3.right, floatingTarget, Mathf.Pow(f, 1.5f));
                        txLeft.localPosition = new Vector3(-heartSplit * (1-f), 0, 0);
                        txRight.localPosition = new Vector3(heartSplit * (1-f), 0, 0);
                        if (f == 1 && synched) {
                            tx.position = floatingTarget;
                            txLeft.gameObject.SetActive(false);
                            txRight.gameObject.SetActive(false);
                            txFull.gameObject.SetActive(leftSide);
                            AdvancePhase();
                        }
                        break;
                    }

                    case 5: {
                        float f = Mathf.Min(phaseTime / (heartType == 3 ? 2f : 0.3f), 1);
                        if (f == 1 && synched) {
                            AdvancePhase();
                        }
                        break;
                    }

                    case 6: {
                        // Shockwave
                        float f = Mathf.Min(phaseTime / 1.5f, 1);
                        float f2 = Mathf.Min(Mathf.Max(0, phaseTime-0.75f) / 0.75f, 1);
                        txFull.localScale = Vector3.one * (1 + Mathf.Pow(f, 0.5f) * 6);
                        //  + new Vector3(0, 0.7f, -1.2f) * (Mathf.Pow(f, 0.5f) * 3);
                        tx.position = Vector3.Lerp(floatingTarget, camTx.position + camTx.forward * 3.0f - camTx.up * 2.0f, Mathf.SmoothStep(0, 1, f));
                        ApplyFade(-1, -1, 1-f2);

                        if (f2 > 0f) {// && !(monster is MMPlayer) && !(other.monster is MMPlayer)) {
                            if (monster is MMMonster) {
                                MMGame.instance.RemoveMonster(monster);
                                monster = null;
                            } else if (monster is MMPlayer) {
                                monster.gameObject.SetActive(false);
                            }
                        }

                        if (f == 1 && synched)
                            AdvancePhase();
                        break;
                    }

                    case 7: {
                        if ((heartType == 3 || ((monster == null || monster is MMPlayer) && (other.monster == null || other.monster is MMPlayer))) && synched) {
                            AdvancePhase();
                        }
                        break;
                    }

                    default: {
                        // Remove monster
                        if (monster is MMPlayer) {
                            // Just wait and end
                            if (phaseTime > 4) {
                                MMGame.instance.FadeOut(true);
                                mode = HeartMode.NONE;
                            }
                        } else {
                            Destroy(gameObject);
                            mode = HeartMode.NONE;
                        }
                        break;
                    }
                }


                break;
            }

        }

    }

    private void FollowMonster(bool stopAtFront = false) {
        if (monster != null) {
            tx.position = monster.transform.position + Vector3.up * hoverHeight;
        }
        float offset = stopAtFront && mode == HeartMode.DESTROYED ? (leftSide ? 180 : 0) : 0;
        float lastR = txRotator.localEulerAngles.y;
        float r = Mathf.Repeat((Time.time - startTime) * 180, 360);
        if (stopAtFront && Mathf.Repeat(r - offset, 360) < Mathf.Repeat(lastR - offset, 360)) {
            frontReached = true;
        }
        if (frontReached)
            r = offset;

        txRotator.localRotation = Quaternion.Euler(0, r, 0);
    }

    private void AdvancePhase() {
        syncCount_ = 0;
        synched_ = false;
        phaseStartTime = Time.time;
        modePhase++;
    }
}
